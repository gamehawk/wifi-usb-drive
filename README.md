# Wifi USB Drive

A Raspberry Pi image preconfigured as per https://silverseams.com/tutorials/converting-a-usb-drive-embroidery-machine-to-wifi/

Details are in that link but: It's just Raspberry Pi OS Lite with a bunch of packages pre-installed so you can connect the USB port to something like an embroidery machine (cut the power line in a USB cable if you power it separately!) and mount the file system over wifi so you can copy files over-the-air.
